const fs = require('fs')
const path = require('path')
const { v4: uuid } = require('uuid')

const postsJSONPath = path.join(__dirname, '..', 'db', 'posts.json')

const updateDB = data => new Promise((resolve, reject) => {
  fs.writeFile(
    postsJSONPath,
    JSON.stringify(data),
    err => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    }
  )
})

module.exports = class Post {
  static async getAllPosts () {
    return new Promise((resolve, reject) => {
      fs.readFile(
        postsJSONPath,
        'utf-8',
        (err, content) => {
          if (err) {
            reject(err)
          } else {
            resolve(JSON.parse(content))
          }
        }
      )
    })
  }

  static async getPostById (id) {
    const posts = await this.getAllPosts()

    return posts.find(post => post.id === id)
  }

  static async deletePost (id) {
    const posts = await this.getAllPosts()

    return updateDB(posts.filter(post => post.id !== id))
  }

  static async updatePost (id, data) {
    const posts = await this.getAllPosts()
    const index = posts.findIndex(post => post.id === id)
    const post = posts[index]
    return updateDB([
      ...posts.slice(0, index),
      { ...post, ...data },
      ...posts.slice(index + 1)
    ])
  }

  static async createPost (data) {
    const posts = await this.getAllPosts()
    const newPost = { ...data, id: uuid() }
    posts.push(newPost)
    await updateDB(posts)

    return newPost
  }
}