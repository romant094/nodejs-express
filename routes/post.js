const Router = require('express')
const Post = require('../models/post')

const router = new Router()

const sendDefaultError = (res, err) => {
  res.status(500).json({ error: err.message })
}

const sendDefaultMessage = (res, message, status = 200) => {
  res.status(status).json({ message, status })
}

router.get('/', async (req, res) => {
  try {
    const posts = await Post.getAllPosts()
    res.status(200).json(posts)
  } catch (err) {
    sendDefaultError(res, err)
  }
})

router.post('/', async (req, res) => {
  try {
    const { author, date, message } = req.body
    if (!author || !date || !message) {
      sendDefaultMessage(res, 'Incorrect data.', 422)
    } else {
      const post = await Post.createPost(req.body)
      res.status(200).json(post)
    }
  } catch (err) {
    sendDefaultError(res, err)
  }
})

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const post = await Post.getPostById(id)
    if (post) {
      await Post.updatePost(id, req.body)
      sendDefaultMessage(res, `Post ${id} was successfully updated.`)
    } else {
      sendDefaultMessage(res, `Post ${id} does not exit.`, 404)
    }
  } catch (err) {
    sendDefaultError(res, err)
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const post = await Post.getPostById(id)
    if (post) {
      await Post.deletePost(id)
      sendDefaultMessage(res, `Post ${id} was successfully deleted.`)
    } else {
      sendDefaultMessage(res, `Post ${id} does not exit.`, 404)
    }
  } catch (err) {
    sendDefaultError(res, err)
  }
})

module.exports = router