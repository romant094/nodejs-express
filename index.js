const express = require('express')

const postRoutes = require('./routes/post')

const port = process.env.PORT || 3000
const apiBase = '/api/v1'

const getApiUrl = path => `${apiBase}${path}`

const app = express()

app.use(express.json())

app.use(getApiUrl('/post'), postRoutes)

app.use('*', (req, res) => {
  if (req.baseUrl === apiBase) {
    res.status(404).send('The endpoint does not exist.')
  } else {
    res.redirect('/')
  }
})

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`)
})